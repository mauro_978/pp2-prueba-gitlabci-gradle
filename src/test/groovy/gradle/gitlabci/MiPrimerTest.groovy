package gradle.gitlabci

import spock.lang.Specification

class MiPrimerTest extends Specification {             // 1

    void 'ver que lib de true'() {            // 2
        given: 'creamos una lib'                    // 3
            def library = new Library()

        when: 'vemos su valor'                          // 4
            def valor = library.someLibraryMethod()

        then: 'el valor es true'              // 5
            valor == true
    }

}
